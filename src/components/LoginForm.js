import React, { Component } from 'react'
import { Text } from 'react-native'
import firebase from 'firebase'
import { Button, Card, CardSection, EditTextWithLabel, Spinner } from './common'

export default class LoginForm extends Component {

    state = {
        email: '',
        password: '',
        error: '',
        isLoading: false,
    }

    render() {
        return (
            <Card>
                <CardSection>
                    <EditTextWithLabel
                        label='Email'
                        value={this.state.email}
                        onChangeText={email => this.setState({ email })}
                        placeholder="user@gmail.com"
                    />
                </CardSection>

                <CardSection >
                    <EditTextWithLabel
                        label='Password'
                        value={this.state.password}
                        onChangeText={password => this.setState({ password })}
                        placeholder="password"
                        secureTextEntry
                    />
                </CardSection>

                {this.renderError()}

                <CardSection>
                    {this.renderButton()}
                </CardSection>
            </Card>
        )
    }

    onButtonPress() {
        const { email, password } = this.state
        this.setState({ error: '', isLoading: true })

        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(this.onLoginSuccess.bind(this))
            .catch(() => {
                firebase.auth().createUserWithEmailAndPassword(email, password)
                    .then(this.onLoginSuccess.bind(this))
                    .catch(this.onLoginFail.bind(this))
            })
    }

    onLoginSuccess() {
        this.setState({
            email: '',
            password: '',
            isLoading: false,
            error: '',
        })
    }

    onLoginFail() {
        this.setState({
            error: 'Authentication Failed.',
            isLoading: false,
        })
    }

    renderError() {
        if (this.state.error != '') {
            return (
                <Text style={styles.errorTextStyle}>
                    {this.state.error}
                </Text>
            )
        } else {
            return (null)
        }
    }

    renderButton() {
        if (this.state.isLoading) {
            return <Spinner size="small" />
        } else {
            return <Button
                onPress={this.onButtonPress.bind(this)}
                title="Login"
            />
        }
    }
}

const styles = {
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: 'red',
    }
}
