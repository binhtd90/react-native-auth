import React from 'react';
import { View } from 'react-native';

const CardSection = (props) => (
        <View style={styles.containterStyle}>
            {props.children}
        </View>
    );

const styles = {
    containterStyle: {
        borderBottomWidth: 1,
        padding: 5,
        borderColor: '#ddd',
        flexDirection: 'row',
        position: 'relative',
    }
};

export { CardSection };
