import React from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';

// components
const Toolbar = (props) => {
    const { textStyle, containerStyle: viewStyle } = styles;

    return (
        <View style={viewStyle}>
            <Text style={textStyle}>{props.title}</Text>
        </View>
    );
};

const styles = StyleSheet.create({ 
    containerStyle: {
        backgroundColor: '#F8F8F8',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.2,
        marginTop: Platform.OS == 'ios' ? 40 : 0,
        elevation: 8,
        position: 'relative',
    },
    textStyle: {
        fontSize: 20,
    }
});

export { Toolbar };
