import React from 'react';
import { TouchableOpacity, Text } from 'react-native';

const Button = ({ onPress, title }) => {
    const {
        buttonStyle,
        textButtonStyle,
    } = styles;

    return (
        <TouchableOpacity
            onPress={onPress}
            style={buttonStyle}
        >
            <Text style={textButtonStyle}>
                {title}
            </Text>
        </TouchableOpacity>
    );
};

const styles = {
    textButtonStyle: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 16,
        fontWeight: '600',
        marginTop: 8,
        paddingBottom: 10,
    },
    buttonStyle: {
        flex: 1,
        alignSelf: 'stretch',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#007aff',
        marginLeft: 8,
        marginRight: 8,
    }
};

export { Button };
