import React, { Component } from 'react';
import { View, YellowBox } from 'react-native';
import { Button, Spinner, Toolbar } from './components/common';
import LoginForm from './components/LoginForm';
import firebase from 'firebase';

class App extends Component {

  state = {
    loggedIn: null,
  }

  componentWillMount() {
    firebase.initializeApp({
      apiKey: "AIzaSyDIi0xk2E1npvPGUgkNf9QAla3bdCUFRTo",
      authDomain: "friendlychat-d6bf1.firebaseapp.com",
      databaseURL: "https://friendlychat-d6bf1.firebaseio.com",
      projectId: "friendlychat-d6bf1",
      storageBucket: "friendlychat-d6bf1.appspot.com",
      messagingSenderId: "309738305669"
    })

    this.checkAuthentication()
  }

  checkAuthentication() {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ loggedIn: true })
      } else {
        this.setState({ loggedIn: false })
      }
    })
  }

  render() {
    return (
      <View style={styles.containerStyle}>
        {YellowBox.ignoreWarnings(["Remote debugger"])}
        <Toolbar title={"Authentication"} />
        {this.renderView()}
      </View>
    )
  }

  renderView() {
    switch (this.state.loggedIn) {
      case true:
        return (
          <View style={styles.buttonContainerStyle}>
            <Button
              onPress={() => firebase.auth().signOut()}
              title="Log out"/>
          </View>
        )
      case false:
        return <LoginForm />
      default:
        return <Spinner size="large" />
    }
  }
}

const styles = {
  containerStyle: {
    flex: 1,
  },
  buttonContainerStyle: {
    height: 40,
    marginTop: 16,
    marginBottom: 16,
  }
}

export default App;